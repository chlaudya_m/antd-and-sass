import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import HomePage from "../views/HomePage";
import MyCountries from "../views/MyCountries";
import LiveStatus from "../component/LiveStatus";


const Routes = () => {
    return (
        <Fragment>
            <Route path="/" exact component={HomePage} />
            <Route path="/:country" exact component={MyCountries} />
            <Route path="/:country/slug" exact component={LiveStatus} />
        </Fragment>
    )
}
export default Routes;