import React, { Fragment, Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class LiveStatus extends Component {
    state = {
        livestatus: []
    }
    getByLivestatus = async () => {
        try {
            const res = await axios.get("https://api.covid19api.com/live/country/south-africa")
            this.setState({ isError: false, isLoading: false, livestatus: res.data })

        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }
    componentDidMount() {
        this.getByLivestatus()
    }
    render() {
        return (
            <Fragment>
                <h1>Live Status</h1>
                {this.state.livestatus.map(live =>
                    <div className="card" key={live.Slug}>
                        <Link to={`/${live.Slug}`}>{live.Country}</Link>
                        <h3>Country : {live.Country}</h3>
                        <h4>Confirmed :{live.Confirmed}</h4>
                        <h4>Deaths :{live.Deaths}</h4>
                        <h4>Recovered :{live.Recovered}</h4>
                        <h4>Active :{live.Active}</h4>
                        <h5>Date :{live.Date}</h5>
                    </div>

                )}
            </Fragment>
        )
    }
}
export default LiveStatus;

