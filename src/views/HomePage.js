import React, { Fragment, Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { Button } from "antd";
import FormPage from "../component/FormPage";
import "../assets/sass/homepage.scss"

class HomePage extends Component {
    // state = {
    //     countries: []
    // }
    // getByCountries = async () => {
    //     const res = await axios.get("https://api.covid19api.com/countries")
    //     this.setState({ countries: res.data })
    // }
    // componentDidMount() {
    //     this.getByCountries()
    // }
    render() {
        return (
            <Fragment>
                <h1 className="h1">Covid 19 API</h1>
                <Button type="primary">Click Me</Button>
                <Button type="primary" danger>Danger</Button>
                <FormPage />
                <div className="product-card">
                    <div className="product-card__image">
                        image
                    </div>
                    <div className="product-card__description">
                        <div className="product-card__description__color">
                            <button className="product-card__description__color--merah">merah</button>
                            <button className="product-card__description__color--biru">biru</button>
                        </div>
                    </div>
                    <h1>test</h1>
                </div>


                {/*                 
                // {this.state.countries.map(country =>
                //     <div key={country.Slug}>
                //         <Link to={`/${country.Slug}`}>{country.Country}</Link>
                //     </div>

                // )} */}
            </Fragment>
        )
    }
}
export default HomePage;