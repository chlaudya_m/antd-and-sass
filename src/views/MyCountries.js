import React, { Fragment, useState } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import LiveStatus from "../component/LiveStatus";


const MyCountries = () => {
    let { country } = useParams();

    return (
        <Fragment>
            <h1>Country Detail:{country}</h1>
            <LiveStatus />
            <Link to="/"><button>Back to home</button></Link>
        </Fragment>
    )
}
export default MyCountries;

